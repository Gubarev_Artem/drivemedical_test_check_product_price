package productprice;


import generalpurpose.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import javax.mail.MessagingException;
import org.testng.*;
import org.testng.annotations.*;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CheckProductPriceTest extends Assert {

    private int number_message = 0;
    private String mail_text = "Test results of product prices" + "\n";
    private ArrayList<String> attachments = new ArrayList<String>();
    private boolean flag_success = false;
    private boolean flag_bad_result = false;


    @AfterClass
    @Parameters("email_address")
    public void tearDown( String address) throws UnsupportedEncodingException, MessagingException {
        if (flag_bad_result  == false) {
            addToEmail( "Success", "Checked all product prices." );
        }
        else addToEmail( "Error", "Not all prices are checked." );
        Email.sendMessage( MainConfig.subject, mail_text, attachments, address );
    }

    @AfterMethod
    public void tearEnd( ) {
        if (flag_success == false) {
            flag_bad_result = true;
        }
    }

    @Parameters( {"start_category_part1", "end_category_part1", "browser"} )
    @Test( description = "Test search and check product images part 1", enabled = true )
    public void testCheckProductPricePart1( int start_category_part1, int end_category_part1, String browser ) throws Exception {
        flag_success = false;
        SeleniumSetting browser_driver = new SeleniumSetting();
        browser_driver.seleniumSetup( browser );
        assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                "Can`t login in DriveMedical with provided credentials");
        CheckPrices( browser_driver, start_category_part1, end_category_part1, browser );
        System.out.println("Part 1 is end");
    }

    @Parameters( {"start_category_part2", "end_category_part2", "browser"} )
    @Test( description = "Test search and check product images part 2", enabled = true )
    public void testCheckProductPricePart2( int start_category_part, int end_category_part, String browser ) throws Exception {
        flag_success = false;
        SeleniumSetting browser_driver = new SeleniumSetting();
        browser_driver.seleniumSetup( browser );
        assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                "Can`t login in DriveMedical with provided credentials");
        CheckPrices( browser_driver, start_category_part, end_category_part, browser );
        System.out.println("Part 2 is end");
    }

    @Parameters( {"start_category_part3", "end_category_part3", "browser"} )
    @Test( description = "Test search and check product images part 3", enabled = true )
    public void testCheckProductPricePart3( int start_category_part, int end_category_part, String browser ) throws Exception {
        flag_success = false;
        SeleniumSetting browser_driver = new SeleniumSetting();
        browser_driver.seleniumSetup( browser );
        assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                "Can`t login in DriveMedical with provided credentials");
        CheckPrices( browser_driver, start_category_part, end_category_part, browser );
        System.out.println("Part 3 is end");
    }

    @Parameters( {"start_category_part4", "end_category_part4", "browser"} )
    @Test( description = "Test search and check product images part 4", enabled = true )
    public void testCheckProductPricePart4( int start_category_part, int end_category_part, String browser ) throws Exception {
        flag_success = false;
        SeleniumSetting browser_driver = new SeleniumSetting();
        browser_driver.seleniumSetup( browser );
        assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                "Can`t login in DriveMedical with provided credentials");
        CheckPrices( browser_driver, start_category_part, end_category_part, browser );
        System.out.println("Part 4 is end");
    }

    @Parameters( {"start_category_part5", "end_category_part5", "browser"} )
    @Test( description = "Test search and check product images part 5", enabled = true )
    public void testCheckProductPricePart5( int start_category_part, int end_category_part, String browser ) throws Exception {
        flag_success = false;
        SeleniumSetting browser_driver = new SeleniumSetting();
        browser_driver.seleniumSetup( browser );
        assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                "Can`t login in DriveMedical with provided credentials");
        CheckPrices( browser_driver, start_category_part, end_category_part, browser );
        System.out.println("Part 5 is end");
    }



    private void CheckPrices( SeleniumSetting browser_driver, int start_category, int end_category, String browser) throws Exception {
        GoToPage.goTogoToMainPageB2B( browser_driver, browser );
        browser_driver.setDriverTimeout();
        // receive products categories
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.left_product_menu_xpath )));
        List<WebElement> list_product_categories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.left_product_menu_xpath ));
        if (list_product_categories.size() == 0) {
            // Add message in report
            System.out.println("Categories product catalog are not found. Checking the product prices is not made. ");
            String screenshot_path = MakeScreenshot.screenshotPageWarning( browser_driver );
            String image_url = browser_driver.driver.getCurrentUrl();
            addToEmail( screenshot_path, "Warning: category products list is empty. Test is stopped.", image_url );
            System.exit(0);
        }
        int amount_product_categories = list_product_categories.size();
        for (int number_category = start_category; number_category < end_category; number_category++) {
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.left_product_menu_xpath )));
            list_product_categories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.left_product_menu_xpath ));
            WebElement category = list_product_categories.get(number_category);
            try {
                category.click();
            }
            catch (org.openqa.selenium.TimeoutException e) {
                browser_driver.restartWebDriver( browser );
                assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                        "Can`t login in DriveMedical with provided credentials");
                CheckPrices( browser_driver, number_category, end_category, browser );
                return;
            }
            try {
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.left_secondary_product_menu_xpath )));
            }
            catch (org.openqa.selenium.TimeoutException e) {
                String screenshot_path = MakeScreenshot.screenshotPageWarning( browser_driver );
                String image_url = browser_driver.driver.getCurrentUrl();
                addToEmail( screenshot_path, "Warning: Product Categories second level are not found. Test is stopped.", image_url );
                System.exit(0);
            }
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.left_secondary_product_menu_xpath )));
            List<WebElement> list_product_subcategories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.left_secondary_product_menu_xpath ));
            if (list_product_subcategories.size() == 0) {
                // Add message in report
                System.out.println("Subcategories product catalog are not found. Checking the product prices is not made. ");
                String screenshot_path = MakeScreenshot.screenshotPageWarning( browser_driver );
                String image_url = browser_driver.driver.getCurrentUrl();
                addToEmail( screenshot_path, "Warning: subcategory products list is empty.", image_url );
                continue;
            }
            int amount_product_subcategories = list_product_subcategories.size();
            for (int number_subcategory = 0; number_subcategory < amount_product_subcategories; number_subcategory++) {
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.left_secondary_product_menu_xpath )));
                list_product_subcategories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.left_secondary_product_menu_xpath ));
                WebElement subcategory = list_product_subcategories.get(number_subcategory);
                try {
                    subcategory.click();
                }
                catch (org.openqa.selenium.TimeoutException e) {
                    browser_driver.restartWebDriver( browser );
                    assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                            "Can`t login in DriveMedical with provided credentials");
                    CheckPrices( browser_driver, number_category, end_category, browser );
                    return;
                }
                browser_driver.wait_browser.until(ExpectedConditions.presenceOfElementLocated(By.xpath(CheckProductPriceData.left_product_menu_xpath )));
                showAllProducts( browser_driver, browser );
                // Get list of products and list of names products
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( MainConfig.title_product_catalog )));
                List<WebElement> list_products = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.products_xpath ));
                if (list_products.size() == 0) {
                    // If is not products.
                    checkPricesInSubdirectoryDepthOfFour(browser_driver, browser);
                }
                else {
                    checkPricesInListProducts(browser_driver, browser);
                }
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.left_product_menu_xpath )));
                list_product_categories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.left_product_menu_xpath ));
                category = list_product_categories.get( number_category );
                try {
                    category.click();
                }
                catch (org.openqa.selenium.TimeoutException e) {
                    browser_driver.restartWebDriver( browser );
                    assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                            "Can`t login in DriveMedical with provided credentials");
                    CheckPrices( browser_driver, number_category, end_category, browser );
                    return;
                }
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated(By.xpath(CheckProductPriceData.left_secondary_product_menu_xpath )));
            }
            try {
                GoToPage.goTogoToMainPageB2B( browser_driver, browser );
            }
            catch (org.openqa.selenium.TimeoutException e) {
                browser_driver.restartWebDriver( browser );
                assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                        "Can`t login in DriveMedical with provided credentials");
                CheckPrices( browser_driver, number_category, end_category, browser );
            }
        }
        browser_driver.driver.close();
        flag_success = true;
        System.out.println("Check part end.");
    }

    private void checkPricesInSubdirectoryDepthOfFour(SeleniumSetting browser_driver, String browser) throws InterruptedException{
        browser_driver.setDriverTimeout();
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.third_level_category_products_xpath )));
        List<WebElement> list_product_subcategories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.third_level_category_products_xpath ));
        if (list_product_subcategories.size() == 0) {
            // Add message in report
            System.out.println("Product subcategories or products are not founded.");
            String screenshot_path= MakeScreenshot.screenshotPageWarning( browser_driver );
            String image_url = browser_driver.driver.getCurrentUrl();
            addToEmail( screenshot_path, "Warning: subcategory products list is empty.", image_url );
            return;
        }
        int amount_subcategories = list_product_subcategories.size();
        // Create list names subcategories
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.names_third_level_category_products_xpath )));
        List<WebElement> list_names_subcategories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.names_third_level_category_products_xpath ));
        String[] names_subcategories = new String[amount_subcategories];
        for(int i=0;i<amount_subcategories;i++) {
            names_subcategories[i] = list_names_subcategories.get(i).getAttribute("title");
        }
        // Check images in subcategories
        String back_url = browser_driver.driver.getCurrentUrl();
        for(int number_subcategory = 0; number_subcategory < amount_subcategories; number_subcategory++) {
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.third_level_category_products_xpath )));
            list_product_subcategories = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.third_level_category_products_xpath ));
            WebElement subcategory = list_product_subcategories.get(number_subcategory);
            try {
                subcategory.click();
            }
            catch (org.openqa.selenium.TimeoutException e) {
                browser_driver.restartWebDriver(browser);
                assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                        "Can`t login in DriveMedical with provided credentials");
                browser_driver.driver.get(back_url);
                checkPricesInSubdirectoryDepthOfFour(browser_driver, browser);
                return;
            }
            List<WebElement> list_products = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.products_xpath));
            if ( list_products.size() == 0) {
                // If is not products.
                checkPricesInSubdirectoryDepthOfFour(browser_driver, browser);
            }
            else {
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.models_products_xpath)));
                showAllProducts( browser_driver, browser );
                checkPricesInListProducts(browser_driver, browser);
            }
            try {
                browser_driver.driver.get(back_url);
            }
            catch (org.openqa.selenium.TimeoutException e) {
                browser_driver.restartWebDriver( browser );
                assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                        "Can`t login in DriveMedical with provided credentials");
                browser_driver.driver.get(back_url);
                checkPricesInSubdirectoryDepthOfFour(browser_driver, browser);
                return;
            }
        }
    }

    private void showAllProducts( SeleniumSetting browser_driver, String browser ) {
        checkSuchProducts( browser_driver );
        // Select show All product. If not go on
        String back_url = browser_driver.driver.getCurrentUrl();
        try {
            browser_driver.driver.findElement( By.xpath( CheckProductPriceData.amount_show_all_product_xpath )).click();
        }
        catch (org.openqa.selenium.TimeoutException e) {
            browser_driver.restartWebDriver( browser );
            assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                    "Can`t login in DriveMedical with provided credentials");
            browser_driver.driver.get(back_url);
            showAllProducts( browser_driver, browser );
            return;
        }
        catch (org.openqa.selenium.NoSuchElementException e) {
            // Does not exist element select All product.
        }
    }

    private void checkPricesInListProducts(SeleniumSetting browser_driver, String browser) throws InterruptedException {
        browser_driver.setDriverTimeout();
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath(  MainConfig.title_product_catalog )));
        checkSuchProducts( browser_driver );
        List<WebElement> list_products = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.products_xpath ));
        if (list_products.size() == 0) {
            // Add message in report
            System.out.println("List products are empty. Checking the images is not made. ");
            String screenshot_path = MakeScreenshot.screenshotPageWarning( browser_driver );
            String image_url = browser_driver.driver.getCurrentUrl();
            addToEmail( screenshot_path, "Warning: products list is empty.", image_url );
            return;
        }
        int amount_products = list_products.size();
        // Create list products names
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.names_products_xpath )));
        List<WebElement> list_name_poducts = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.names_products_xpath ));
        String[] names_products = new String[amount_products];
        for(int i = 0;i < amount_products; i++) {
            names_products[i] = list_name_poducts.get(i).getAttribute( "title").trim();
        }
        // Create list products models
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.models_products_xpath )));
        List<WebElement> list_products_models = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.models_products_xpath ));
        String[] products_models = new String[amount_products];
        for(int i= 0 ;i < amount_products; i++) {
            products_models[i] = list_products_models.get(i).getText().trim();
        }
        // Check images in list products
        String back_url = browser_driver.driver.getCurrentUrl();
        for (int number_product = 0; number_product < amount_products; number_product++) {
            checkSuchProducts( browser_driver );
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.products_xpath )));
            list_products = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.products_xpath ));
            WebElement product = list_products.get(number_product);
            // Enter in product page
            try {
                product.click();
            }
            catch (org.openqa.selenium.TimeoutException e) {
                browser_driver.restartWebDriver( browser );
                assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                        "Can`t login in DriveMedical with provided credentials");
                browser_driver.driver.get(back_url);
                checkPricesInListProducts(browser_driver, browser);
                return;
            }
            browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.header_product_xpath )));
            // Check all prices on the page
            List<WebElement> list_buttons_buy = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.button_buy_now_xpath ));
            if (list_buttons_buy.size() == 0) {
                // Add message in report
                System.out.println("No prices on the product page.");
                String screenshot_path = MakeScreenshot.screenshotPage( "Product: " + names_products[number_product], browser_driver );
                String image_url = browser_driver.driver.getCurrentUrl();
                addToEmail( names_products[number_product], screenshot_path, products_models[number_product],image_url );
                // Back to product list
                try {
                    browser_driver.driver.get(back_url);
                }
                catch (org.openqa.selenium.TimeoutException e) {
                    browser_driver.restartWebDriver( browser );
                    assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                            "Can`t login in DriveMedical with provided credentials");
                    browser_driver.driver.get(back_url);
                    return;
                }
                continue;
            }
            int amount_button_price = list_buttons_buy.size();
            List<WebElement> list_product_sku = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.product_sku_xpath ));
            String[] sku_products = new String[amount_button_price];
            for (int i = 0; i < amount_button_price; i++) {
                sku_products[i] = list_product_sku.get(i).getText().trim();
            }
            // Store the current window handle
            String winHandleBefore = browser_driver.driver.getWindowHandle();
            String product_url = browser_driver.driver.getCurrentUrl();
            for (int number_button = 0; number_button < amount_button_price; number_button++) {
                browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckProductPriceData.header_product_xpath )));
                list_buttons_buy = browser_driver.driver.findElements( By.xpath( CheckProductPriceData.button_buy_now_xpath ));
                WebElement button_price = list_buttons_buy.get(number_button);
                try {
                    button_price.click();
                    for ( String winHandle : browser_driver.driver.getWindowHandles()) {
                        browser_driver.driver.switchTo().window(winHandle);
                    }
                }
                catch (org.openqa.selenium.TimeoutException e) {
                    browser_driver.restartWebDriver( browser );
                    assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                            "Can`t login in DriveMedical with provided credentials");
                    browser_driver.driver.get(back_url);
                    checkPricesInListProducts(browser_driver, browser);
                    return;
                }
                //Check price
                try {
                    String price = browser_driver.driver.findElement( By.xpath( CheckProductPriceData.price_xpath )).getText();
                    Pattern p = Pattern.compile("(\\$[0-9]+.[0-9]{2})");
                    Matcher m = p.matcher(price);
                    boolean b = m.matches();
                    if (!b) {
                        // Add message in report
                        String screenshot_path = MakeScreenshot.screenshotPage( "Product: " + names_products[number_product], browser_driver );
                        if (list_product_sku.size() == 0) {
                            addToEmail( names_products[number_product], screenshot_path, products_models[number_product],product_url, "" );
                        }
                        else {
                            addToEmail( names_products[number_product], screenshot_path, products_models[number_product],product_url, sku_products[number_button] );
                        }
                    }
                }
                catch (org.openqa.selenium.NoSuchElementException e) {
                    // Add message in report
                    String screenshot_path = MakeScreenshot.screenshotPage( "Product: " + names_products[number_product], browser_driver );
                    addToEmail( names_products[number_product], screenshot_path, products_models[number_product],product_url, sku_products[number_button] );
                }
                // Out from popup window
                browser_driver.driver.findElement( By.xpath( CheckProductPriceData.button_close_window_xpath )).click();
                // Switch back to original browser (first window)
                browser_driver.driver.switchTo().window(winHandleBefore);
                // Check visibility main page
                boolean b;
                 do {
                    Thread.sleep(1000);
                    b = browser_driver.driver.findElement( By.xpath( CheckProductPriceData.button_buy_now_xpath)).isDisplayed();
                }
                while (!b);
            }
            // back to product list
            try {
                browser_driver.driver.get(back_url);
            }
            catch (org.openqa.selenium.TimeoutException e) {
                browser_driver.restartWebDriver( browser );
                assertTrue( LoginLogout.loginDriveMedical( browser_driver, MainConfig.login_consumer, MainConfig.password_consumer, browser ),
                        "Can`t login in DriveMedical with provided credentials");
                browser_driver.driver.get(back_url);
                checkPricesInListProducts(browser_driver, browser);
                return;
            }
        }
    }

    private void addToEmail( String product_name, String screenshot_path, String product_model, String url, String sku ) {
        mail_text = mail_text + "Product price is not displayed: " + "\n";
        number_message++;
        attachments.add( screenshot_path );
        mail_text = mail_text + number_message + ". " + product_name + "\n" + product_model + "\n" + "Item:" + "\n" + sku + "\n" + "URL: " + url + "\n\n";
    }

    private void addToEmail( String product_name, String screenshot_path, String product_model, String url ) {
        mail_text = mail_text + "Product price is not displayed: " + "\n";
        number_message++;
        attachments.add( screenshot_path );
        mail_text = mail_text + number_message + ". " + product_name + "\n" + product_model + "\n" + "Item:" + "\n" + "URL: " + url + "\n\n";
    }

    private void addToEmail( String screenshot_path, String event, String url ) {
        attachments.add( screenshot_path );
        number_message++;
        mail_text = mail_text + number_message + ". " + event + "\n" + "URL: " + url + "\n\n";
    }

    private void addToEmail( String event ) {
        number_message++;
        mail_text = mail_text + number_message + ". " + event + "\n" + "\n\n";
    }

    private void addToEmail( String flag, String event) {
        attachments.add( flag );
        mail_text = mail_text + "\n " + event + "\n";
    }



    private void checkSuchProducts( SeleniumSetting browser_driver ) {
        browser_driver.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        try {
            browser_driver.driver.findElement( By.xpath( CheckProductPriceData.message_no_product_xpath ));
            String url = browser_driver.driver.getCurrentUrl();
            browser_driver.driver.get(url);
            checkSuchProducts( browser_driver );
        }
        catch (Exception e) {
            browser_driver.driver.manage().timeouts().implicitlyWait(MainConfig.default_timeout, TimeUnit.SECONDS);
            return;
        }
        browser_driver.driver.manage().timeouts().implicitlyWait(MainConfig.default_timeout, TimeUnit.SECONDS);
    }
}
