package productprice;

public class CheckProductPriceData {

    public static String left_product_menu_xpath = "//ul[@id='business-menu']//a[@title]/span";
    public static String left_secondary_product_menu_xpath = "//div[@class='n2-banner-right']/ul/li/a";
    public static String amount_show_all_product_xpath = "//select/option[contains(text(),'All')]";
    public static String header_product_xpath = "//div[@class='product-name']/h1";
    public static String models_products_xpath = "//span[@class = 'price']/span";
    public static String button_buy_now_xpath = "//span[@class='pdp-block pdp-btn-desc']";
    public static String price_xpath = "//div[contains (@class,'link-myprice')]";
    public static String button_close_window_xpath = "//a[@id='fancybox-close']";
    public static String product_sku_xpath = "//td[@class='product-sku']";
    public static String product_alt_sku_xpath = "//td[@class='product-att']";

    public static String message_no_product_xpath = "//p[@class='note-msg'][contains (text(), 'There are no products matching the selection.')]";
    public static String third_level_category_products_xpath = "//div[@class='product-image']/a/img";
    public static String names_third_level_category_products_xpath = "//h6/a";
    public static String products_xpath = "//ol[@id='products-list']//a/img";
    public static String names_products_xpath = "//ol[@id='products-list']//h2/a";
}
