package generalpurpose;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;



public class GoToPage {
    public static void goTogoToMainPageB2B( SeleniumSetting browser_driver, String browser ) {
        try {
            browser_driver.driver.get( MainConfig.main_page_b2b_url );
        }
        catch (org.openqa.selenium.TimeoutException e) {
            browser_driver.driver.close();
            browser_driver.seleniumSetup( browser );
            GoToPage.goTogoToMainPageB2B( browser_driver, browser );
            return;
        }
        browser_driver.wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( MainConfig.title_product_catalog)));
    }
}
